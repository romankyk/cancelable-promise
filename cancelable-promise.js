class CancelablePromise {
  constructor(executor, prevPromise = this) {  
    if (typeof executor !== 'function') {
      throw new Error();
    }

    this.state = 'pending';
    this.value = undefined;
    this.reason = undefined;
    this.onFulfilledCallbacks = [];
    this.onRejectedCallbacks = [];
    this.isCanceled = false;

    const resolve = (value) => {
      if (this.state === 'pending') {
        this.state = 'fulfilled';
        this.value = value;
        this.onFulfilledCallbacks.forEach((callback) => callback(value));
      }
    };

    try {
      executor(resolve, this.reject);
    } catch (error) {
      reject(error);
    }

    this.prevPromise = prevPromise;
  }

  reject = (reason) => {
    if (this.state === 'pending') {
      this.state = 'rejected';
      this.reason = reason;
      this.onRejectedCallbacks.forEach((callback) => callback(reason));
    }
  };

  then(onFulfilled, onRejected) {
    const isOnFulfilledPresentAndFunction = typeof onFulfilled !== 'function' && onFulfilled
    const isOnRejectedPresentAndFunction = typeof onRejected !== 'function' && onRejected

    if (isOnFulfilledPresentAndFunction || isOnRejectedPresentAndFunction) {
      throw new Error('CancelablePromise.then() should have arguement!');
    }

    const nextPromise = new CancelablePromise((resolve, reject) => {
      const fulfillCallback = (value) => {
        try {
          if (typeof onFulfilled !== 'function') {
            resolve(value);
          }

          const result = onFulfilled(value);
          if (result instanceof CancelablePromise || result instanceof Promise) {
            result.then(resolve, reject);
          } else {
            resolve(result);
          }
        } catch (error) {
          reject(error);
        }
      };

      const rejectCallback = (reason) => {
        try {
          if (typeof onRejected === 'function') {
            resolve(onRejected(reason));
          } else {
            reject(reason);
          }
        } catch (error) {
          reject(error);
        }
      };

      switch (this.state) {
        case ('fulfilled'): 
          fulfillCallback(this.value);
          break;

        case ('rejected'):
          rejectCallback(this.reason);
          break;

        default:
          this.onFulfilledCallbacks.push(fulfillCallback);
          this.onRejectedCallbacks.push(rejectCallback);
      }
    }, this);

    this.nextPromise = nextPromise;

    return nextPromise;
  }

  cancel = () => {
    if (this.isCanceled) {
      return;
    }

    this.isCanceled = true;
    this.reject({ isCanceled: true });
    if (this !== this.prevPromise) {
      this.prevPromise.cancel();
      this.onFulfilledCallbacks = [];
      this.onRejectedCallbacks = []; 
    }

    if (this.nextPromise) {
      this.nextPromise.cancel();
    }
  };

  catch(onRejected) {
    return this.then(undefined, onRejected);
  }

  static resolve(value) {
    return new CancelablePromise((resolve) => resolve(value));
  }

  static reject(reason) {
    return new CancelablePromise((_, reject) => reject(reason));
  }
}

module.exports = CancelablePromise;
